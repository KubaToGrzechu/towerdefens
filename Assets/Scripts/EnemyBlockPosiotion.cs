﻿using UnityEngine;

[ExecuteInEditMode]
public class EnemyBlockPosiotion : MonoBehaviour {
    Vector3 place;
    [SerializeField] private int wysokosc = -5;
	// Update is called once per frame
	void Update () {

        place = new Vector3Int(
            Mathf.RoundToInt(transform.position.x / 10)*10,
            wysokosc,
            Mathf.RoundToInt(transform.position.z / 10)*10
            );
        transform.position = place;
    }
}
