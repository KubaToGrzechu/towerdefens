﻿using System;
using UnityEngine;

public class Tower : MonoBehaviour {

    [SerializeField] Transform objectToPan;
    
    [SerializeField] float attackRange=5f;
    [SerializeField] ParticleSystem projectileParticle;
    

     //turret standing
    public WayPoints _wayPoint;
    //state of each tower 
    Transform targetEnemy;
    // Update is called once per frame
    void Update ()
    {

        SetTargetEnemy();
        if (targetEnemy)
        {
            objectToPan.LookAt(targetEnemy);
            FireAtEnemy();
        }else
        {
            Shoot(false);
        }
	}

    
    private void FireAtEnemy()
    {
        float distansToEnemy = Vector3.Distance(targetEnemy.transform.position, gameObject.transform.position);
       // Debug.Log(distansToEnemy);
        if (distansToEnemy <= attackRange)
        {
            Shoot(true);
        }
        else
        {
            Shoot(false);
        }
    }

    private void Shoot(bool isShooting)
    {
        ParticleSystem.EmissionModule emissionModule = projectileParticle.emission;

        emissionModule.enabled = isShooting;
    }

    private void SetTargetEnemy()
    {
        var sceneEnemies = FindObjectsOfType<EnemyDMG>();
        if (sceneEnemies.Length == 0) { return; }

        Transform closestEnemy = sceneEnemies[0].transform;

        foreach (EnemyDMG testEnemy in sceneEnemies)
        {
            closestEnemy = GetClosest(closestEnemy, testEnemy.transform);
        }
        targetEnemy = closestEnemy;

    }

    private Transform GetClosest(Transform closestEnemy, Transform testEnemyPos)
    {
        var distansA = Vector3.Distance(closestEnemy.position, gameObject.transform.position);
        var distansB = Vector3.Distance(testEnemyPos.position, gameObject.transform.position);

        if (distansA > distansB)
        {
            return testEnemyPos;
        }
        else
        {
            return closestEnemy;
        }
    }
}
