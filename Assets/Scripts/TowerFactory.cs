﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour {

    [SerializeField] private Tower turet;
    [SerializeField] private int turetLimit;
    [SerializeField] private Vector3 position;
    [SerializeField] Transform towerParent;

    Queue<Tower> towers = new Queue<Tower>();

    public void AddTower(WayPoints baseWaypoint)
    {
        Vector3 position = new Vector3(baseWaypoint.transform.position.x, baseWaypoint.transform.position.y + 5f, baseWaypoint.transform.position.z);

        if (towers.Count >= turetLimit)
        {
            MovingExistingTower(baseWaypoint, position);
        }
        else
        {
            InstantiateNewTower(baseWaypoint, position);
        }
    }

    private void MovingExistingTower(WayPoints baseWaypoint,Vector3 position)
    {
        
        Tower oldTower = towers.Dequeue();

        oldTower._wayPoint.isPlaceable = true;
        oldTower._wayPoint = baseWaypoint;
        oldTower._wayPoint.isPlaceable = false;
        // Debug.Log("You cant place more then: " + turetLimit);
        // Debug.Log(oldTower.transform.position + ":old Position " + position + ": new Position ");
        oldTower.transform.position = position;
        towers.Enqueue(oldTower);
        
    }

    private void InstantiateNewTower(WayPoints baseWaypoint, Vector3 position)
    {
        
        var newTower = Instantiate(turet, position, Quaternion.identity);
        newTower.transform.parent = towerParent;
        baseWaypoint.isPlaceable = false;
        newTower.GetComponent<Tower>()._wayPoint = baseWaypoint;
        towers.Enqueue(newTower);
    }

   
}
