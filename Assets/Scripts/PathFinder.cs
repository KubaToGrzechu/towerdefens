﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder : MonoBehaviour {

    [SerializeField] public WayPoints startPoint, endPoint;
    public Dictionary<Vector2Int, WayPoints> grid = new Dictionary<Vector2Int, WayPoints>();
    Queue<WayPoints> queue = new Queue<WayPoints>();
    public WayPoints searchCenter;
    bool isRunning = true;
    Vector2Int[] directions =
    {
        Vector2Int.up,
        Vector2Int.down,
        Vector2Int.left,
        Vector2Int.right,
    };
    private List<WayPoints> path = new List<WayPoints>();
    public List<WayPoints> GetPath()
    {
        if (path.Count == 0)
        {
            CalculatePath();
        }
        return path;
    }

    private void CalculatePath()
    {
        LoadBlocks();
        BreadthFirstSearch();
        CreatPath();
    }


    private void LoadBlocks()
    {
        var waypoints = FindObjectsOfType<WayPoints>();

        foreach (WayPoints waypoint in waypoints)
        {
            var gridPos = waypoint.GetGridPos();
            bool isOverlapping = grid.ContainsKey(gridPos);
            if(isOverlapping)
            {
                Debug.LogWarning("Over Lapping");
            }
            else
            {
                grid.Add(waypoint.GetGridPos(), waypoint);
               // Debug.Log(waypoint.name);
            }
        }

    }   
    private void BreadthFirstSearch()
    {
        queue.Enqueue(startPoint);

        while(queue.Count > 0 && isRunning)
        {
            searchCenter = queue.Dequeue();
            HaltIfEndFound();
            FindingNeighbors();
            searchCenter.isExplore = true;
        }

    }

    private void HaltIfEndFound()
    {
       if(searchCenter == endPoint)
        {
           // print("Seraching from end node.");
            isRunning = false;
        }
    }

    private void FindingNeighbors()
    {
        if(!isRunning) { return; }

        foreach (Vector2Int direction in directions)
        {
            Vector2Int neighbourCoordinates = searchCenter.GetGridPos() + direction;
            
            if (grid.ContainsKey(neighbourCoordinates))
            {
                QueueNewNeighbours(neighbourCoordinates);
            }
            else
            {
               // print("Nie ma");
            }
        }
    }

    private void QueueNewNeighbours(Vector2Int neighbourCoordinates)
    {
        WayPoints neighbour = grid[neighbourCoordinates];

        if(neighbour.isExplore || queue.Contains(neighbour))
        {
        }
        else
        {  
            if(neighbour != endPoint)
            {
                neighbour.SetTopMaterial();
            }
                queue.Enqueue(neighbour);
                neighbour.exploredFrom = searchCenter;

           
        }
    }

    private void CreatPath()
    {
        SetAsPath(endPoint);

        WayPoints previous = endPoint.exploredFrom;

        while(previous != startPoint)
        {
            
            SetAsPath(previous);
            previous = previous.exploredFrom;
        }

        SetAsPath(startPoint);

        path.Reverse();
    }
    private void SetAsPath(WayPoints waypoint)
    {
        path.Add(waypoint);
        waypoint.isPlaceable = false;
    }
}
