﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[SelectionBase]
public class CubeEditor : MonoBehaviour {

    //[Range(1, 20)][SerializeField] private 

    

    
 
    WayPoints waypoint;
    int gridSize;

    private void Awake()
    {
        waypoint = GetComponent<WayPoints>();
    }

    void Update()
    {
        UpdateLabel();

        int gridSize = waypoint.GetGridSize();
        transform.position = new Vector3(
            waypoint.GetGridPos().x * gridSize,
            0f,
            waypoint.GetGridPos().y * gridSize
        );
    }

    


    private void UpdateLabel()
    {

        TextMesh textMesh = GetComponentInChildren<TextMesh>();
       
        string lableText = waypoint.GetGridPos().x + "," + waypoint.GetGridPos().y;
        textMesh.text = lableText;
        gameObject.name = "Cube [" + lableText + "]";
    }
}
