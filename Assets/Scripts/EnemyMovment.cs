﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovment : MonoBehaviour {

    [SerializeField] float movementPeriod = .5f;
    [SerializeField] ParticleSystem goalParticle;
    

    void Start ()
    {
        
        PathFinder pathfinder = FindObjectOfType<PathFinder>();
        var path = pathfinder.GetPath();
        StartCoroutine(FollowPath(path));
    }

    IEnumerator FollowPath(List<WayPoints> path)
    {
     
        foreach (WayPoints cube in path)
        {
            Vector3 newPositionV3 = new Vector3(cube.transform.position.x, transform.position.y, cube.transform.position.z);
            transform.position = newPositionV3;
            yield return new WaitForSeconds(movementPeriod);
        }
        SelfDestruct();
    }

    private void SelfDestruct()
    {
        var vfx = Instantiate(goalParticle, new Vector3 (transform.position.x, transform.position.y+5f, transform.position.z+5f), Quaternion.identity);
        vfx.Play();
        Destroy(vfx.gameObject, vfx.main.duration);
        Destroy(gameObject);
    }
}
