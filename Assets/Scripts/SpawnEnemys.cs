﻿using System.Collections;

using UnityEngine;
using UnityEngine.UI;

public class SpawnEnemys : MonoBehaviour {

    [SerializeField] private float secondBetweenSpawns = 2f;
    [SerializeField] private EnemyMovment enemy;
    [SerializeField] Text _howManyenemys;
    private int score;
    
	
	// Update is called once per frame
	void Start () {

        _howManyenemys.text = score.ToString();

        StartCoroutine(SpawnEnemy(enemy));
	}
    IEnumerator SpawnEnemy(EnemyMovment enemy)
    {
        while(true)
        {
            score++;
            _howManyenemys.text = score.ToString();

            //Vector3 posiotion = new Vector3(gameObject.transform.position.x, enemy.transform.position.y, gameObject.transform.position.z);
            var enemySpwan = Instantiate(enemy, transform);
            enemySpwan.transform.parent = gameObject.transform;
            yield return new WaitForSeconds(secondBetweenSpawns);
        }
    }
}
