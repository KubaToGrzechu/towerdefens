﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDMG : MonoBehaviour {

    [SerializeField] int HP = 10;
    [SerializeField] ParticleSystem hitEfect;
    [SerializeField] ParticleSystem deathEfect;
   // [SerializeField] GameObject hitEfect;
	void Start () {
		
	}
    private void OnParticleCollision(GameObject other)
    {
        
        ProcesHit();
        if (HP <= 0)
        {
            var vfx = Instantiate(deathEfect, new Vector3(transform.position.x,transform.position.y + 5, transform.position.z), Quaternion.identity);
            vfx.Play();
            float destroyDelay = vfx.main.duration;
            Destroy(vfx.gameObject, destroyDelay);
            Destroy(gameObject);
        }

    }
    void ProcesHit()
    {
       
        hitEfect.Play();
        HP--;
       
    }
}
