﻿using UnityEngine;
using UnityEngine.UI;

public class BaseHP : MonoBehaviour {

    [SerializeField] int _HP = 10;
    [SerializeField] int _HpDecrease = 1;
    [SerializeField] Text _HPTEXT;

    private void Start()
    {
        _HPTEXT.text = _HP.ToString();
    }
    private void OnTriggerEnter(Collider other)
    {
        _HP -= _HpDecrease;
        _HPTEXT.text = _HP.ToString();
        //Debug.Log(_HP);
    }
}
